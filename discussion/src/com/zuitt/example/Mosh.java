package com.zuitt.example;

import java.util.Scanner;

public class Mosh {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        final byte MONTH_IN_YEAR  = 12;
        final byte PERCENT = 100;

        System.out.print("Enter Principal: ");
        int principal = input.nextInt();

        System.out.print("Enter Annual interest rate: ");
        float annualInterest = input.nextFloat();
        float monthlyInterest = annualInterest / PERCENT / MONTH_IN_YEAR;

        System.out.print("Enter Period(Years): ");
        int year = input.nextInt();
        int numberOfYear = year * MONTH_IN_YEAR;

        double mortgage = principal
                * (monthlyInterest * Math.pow(1 + monthlyInterest, numberOfYear))
                / (Math.pow(1 + monthlyInterest, numberOfYear) -1);

        System.out.println("Mortgage: " + mortgage);


    }
}
