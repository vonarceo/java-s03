package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {
    public static void main(String[] args){
        //[LOOPS]
            // are control structures that allows code blocks to be executed multiple time.

        //while loop

//        int x = 0;
//
//        while(x < 10){
//            System.out.println("Hello World count: " + x);
//            x++;
//        }

        // do while
//        int y = 10;
//        do {
//            System.out.println("Countdown: " + y);
//            y--;
//        } while (y > 10);
//
        // for loop

//        for (int i = 0; i < 10; i++){
//            System.out.println("Von Arceo" + i);
//        }
//
        // for loop with array
//
//        int[] intArray = {100, 200, 300, 400, 500};
//        for (int i = 0; i < intArray.length; i++){
//            System.out.println(intArray[i]);
//        }

        // for loop with array (2nd)

        /*
        * for(dataType itemName(singular) : arrayName(plural) ){
        * // code block
        *
        *
        *
        * */

//        String[] nameArray = {"John", "Paul", "George", "Ringo"};
//        // this get each element of the array and assign it to the variable called name
//
//        for(String name : nameArray){
//            System.out.println(name);
//        }

        // nested loops with multi dimensional arrays

        String[][] classroom = new String[3][3];

        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        // outer loop that will loop through the rows.
//        for(int row = 0; row < 3; row++){
//            //inner loop that will loop through the rows
//            for (int col = 0; col < 3; col++){
//                System.out.println(classroom[row][col]);
//            }
//
//        }

        // for-each with multi dimensional arrays

//        for (String[] row: classroom){
//            for (String column: row){
//                System.out.println(column);
//            }
//        }

        //forEach ArrayList;
        /*Synthax:
        * arrayListName.forEach(Consumer<Element>) -> //code block;
        * "->" this is called lambda operator which is used to seperate the parameter and implementation;
        *
        *
        * */

        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);

//        numbers.forEach(num -> System.out.println("ArrayList of Numbers: " + num));

        //forEach HashMaps
        // Synthax:
        //hashMapName.forEach(key, value) -> // code block;
        HashMap<String, Integer> grades = new HashMap<String, Integer>(){{
            put("English", 90);
            put("Math", 95);
            put("Science", 97);
            put("History", 94);
        }};

        grades.forEach((subject, grade) -> System.out.println("The student grade for " + subject + " is " + grade));




    }
}
